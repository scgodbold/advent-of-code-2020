use std::fs::File;
use crate::lib::AdventDay;

struct Day {}

pub fn new() -> &'static impl AdventDay {
    Day::new()
}

impl AdventDay for Day {
    fn part1(&self, input: &mut File) -> String {
        "unimplemented".to_string()
    }
    fn part2(&self, input: &mut File) -> String {
        "unimplemented".to_string()
    }
}

impl<'a> Day {
    pub fn new() -> &'a Self{
        &Self{}
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
