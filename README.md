# Advent of Code 2k20

My solutions for as far as I make in in rust. Still learning this language.

## Usage

```
aoc-2k20-run 0.1.0

USAGE:
    aoc-2k20 run [OPTIONS] <day>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -i, --inputdir <inputdir>     [default: inputs]
    -p, --parts <parts>           [default: all]

ARGS:
    <day>
```
## Framework

Took inspiration for my framework from [Kushagra-0801](https://github.com/Kushagra-0801/adventofcode-rs)
Since I am just picking up the language I ended up rewriting it all following their codebase as a template. Really helped connect some dots on how rusts works for me before getting started.
