use std::io::{BufRead, BufReader, Seek, SeekFrom};
use std::fmt::Display;
use std::fs::File;
use std::error::Error;

#[derive(Debug)]
#[allow(dead_code)]
pub enum AdventError {
    NoAnswerFound,
    NotImplemented,
    InputNotFound,
}

impl Display for AdventError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AdventError::NoAnswerFound => write!(f, "AdventError::NoAnswerFound"),
            AdventError::NotImplemented => write!(f, "AdventError::NotImplemented"),
            AdventError::InputNotFound => write!(f, "AdventError::InputNotFound"),
        }
    }
}

impl Error for AdventError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(self)
    }
}

pub trait AdventDay {
    fn part1(&self, input: &mut File) -> String;
    fn part2(&self, input: &mut File) -> String;
    fn all(&self, input: &mut File) -> String {
        let p1 = self.part1(input);

        let p2 = self.part2(input);

        format!("Part 1:\t\t{}\n\
                 Part 2:\t\t{}\n",
                 p1, p2)
    }

    fn read_input(&self, input: &mut File) -> Result<Vec<String>, AdventError> {
        match input.seek(SeekFrom::Start(0)) {
            Ok(_) => {
                let reader = BufReader::new(input);
                let lines = reader.lines();
                let mut entries: Vec<String> = Vec::new();
                for line in lines {
                    if let Ok(value) = line {
                        let entry: String = value.parse().unwrap();
                        entries.push(entry);
                    }
                }
                Ok(entries)
            },
            Err(_) => Err(AdventError::InputNotFound),
        }
    }
}
