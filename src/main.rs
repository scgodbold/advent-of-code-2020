extern crate dotenv;
extern crate regex;
extern crate reqwest;
extern crate structopt;

use structopt::StructOpt;

mod run;
mod day;
mod lib;
mod days;
mod init;

use run::Run;
use init::Init;

#[derive(StructOpt, Debug)]
#[structopt(name = "Advent of Code: 2k20")]
enum Args {
    Run(Run),
    Init(Init),
}

fn main() -> Result<(), std::io::Error> {
    let args = Args::from_args();
    match args {
        Args::Run(run) => {
            let out = run.run()?;
            println!("{}", out);
        },
        Args::Init(init) => {
            let out = init.init()?;
            println!("{}", out)
        }
    };
    Ok(())
}
