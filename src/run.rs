use structopt::StructOpt;
use std::str::FromStr;
use std::default::Default;
use std::fmt::Display;
use std::path::Path;
use std::fs::File;

use crate::day::Day;


#[derive(StructOpt, Debug)]
pub struct Run {
    day: Day,

    #[structopt(short, long, default_value)]
    parts: Part,

    #[structopt(short, long, default_value = "inputs")]
    inputdir: String,
}

#[derive(StructOpt, Debug)]
enum Part {
    Part1,
    Part2,
    All,
}

impl FromStr for Part {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "part1" | "1" => Ok(Self::Part1),
            "part2" | "2" => Ok(Self::Part2),
            "all" => Ok(Self::All),
            _ => Err("Unknown part specified")
        }
    }
}

impl Default for Part {
    fn default() -> Self { Self::All }
}

impl Display for Part {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Part::Part1 => write!(f, "part1"),
            Part::Part2 => write!(f, "part2"),
            Part::All => write!(f, "all"),
        }
    }
}

impl Run {
    pub fn run(&self) -> Result<String, std::io::Error> {
        let path = Path::new(&self.inputdir).join(format!("day{}.txt", self.day.get()));
        let mut file = File::open(path)?;

        let code = self.day.get_code();
        let output = match self.parts {
           Part::Part1 => code.part1(&mut file),
           Part::Part2 => code.part2(&mut file),
           Part::All => code.all(&mut file),
        };
        Ok(output)
    }
}
