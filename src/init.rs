use std::fs;
use dotenv;
use reqwest;
use structopt::StructOpt;

use crate::day::Day;

#[derive(StructOpt, Debug)]
pub struct Init {
    day: Day,

    #[structopt(short, long, default_value = "inputs")]
    inputdir: String,
}

impl Init {
    pub fn init(&self) -> Result<String, std::io::Error> {
        dotenv::dotenv().ok();
        let token: String;
        match dotenv::var("AOC_SESSION_TOKEN") {
            Ok(v) => token = v,
            Err(_) => return Ok("Unable to find session token".to_string()),
        }
        println!("Initializing {0} into {1}/day{0}.txt", self.day.get(), self.inputdir);

        match fs::write(format!("{}/day{}.txt", self.inputdir, self.day.get()), self.fetch_input(token)) {
            Ok(_) => Ok(format!("Day {} successfully initialized", self.day.get())),
            Err(_) => Ok("Unable to fetch input".to_string()),
        }
    }

    fn fetch_input(&self, token: String) -> String {
        let client = reqwest::blocking::Client::new();
        let url = reqwest::Url::parse(format!("https://adventofcode.com/2020/day/{}/input", self.day.get()).as_str()).unwrap();
        client.get(url)
            .header("Cookie", format!("session={}", token))
            .send()
            .unwrap()
            .text()
            .unwrap()
    }
}
