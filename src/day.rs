use structopt::StructOpt;
use std::str::FromStr;
use std::convert::TryInto;

use crate::lib::AdventDay;
use crate::days;

#[derive(StructOpt, Debug)]
pub struct Day {
    day: u8,
}

impl FromStr for Day {
    type Err = &'static str;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let day: u8 = s.parse().map_err(|_| "Day must be an integer")?;
        if day < 1 || day > 25 {
            Err("Day must be between 1 and 25")
        } else {
            let day = day.try_into().unwrap();
            Ok(Self{ day })
        }
    }
}

impl Day {
    pub fn get(&self) -> u8 {
        self.day
    }

    pub fn get_code(&self) -> &dyn AdventDay {
        match self.get() {
            1 => days::one::new(),
            2 => days::two::new(),
            3 => days::three::new(),
            4 => days::four::new(),
            5 => days::five::new(),
            6 => days::six::new(),
            7 => days::seven::new(),
            8 => days::eight::new(),
            9 => days::nine::new(),
            10 => days::ten::new(),
            _ => panic!(format!("Unknown Day specified: {}", self.day))
        }
    }
}
