use std::str::FromStr;
use std::clone::Clone;
use std::collections::HashMap;
use std::fs::File;
use crate::lib::AdventDay;

struct Day {}

#[derive(Debug, PartialEq, Clone, Copy)]
enum Code {
    NoOp(i32),
    Jump(i32),
    Acc(i32),
}

#[derive(Debug)]
struct Program {
    pointer: i32,
    accumulator: i32,
    original: Vec<Code>,
    code: Vec<Code>,
    pos_count: HashMap<usize, i32>,
}


pub fn new() -> &'static impl AdventDay {
    Day::new()
}

impl AdventDay for Day {
    fn part1(&self, input: &mut File) -> String {
        let mut prog = Program::from(&self.read_input(input).unwrap());
        prog.run_one();
        format!("{}", prog.accumulator)
    }
    fn part2(&self, input: &mut File) -> String {
        let mut prog = Program::from(&self.read_input(input).unwrap());
        prog.fix();
        format!("{}", prog.accumulator)
    }
}

impl<'a> Day {
    pub fn new() -> &'a Self{
        &Self{}
    }
}

impl FromStr for Code {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split: Vec<&str> = s.split(' ').collect();
        let val: i32;
        if let Ok(v) = split[1].parse::<i32>() {
            val = v;
        } else {
            return Err("couldnt parse value attached to code")
        }
        match split[0] {
            "acc" => Ok(Code::Acc(val)),
            "jmp" => Ok(Code::Jump(val)),
            "nop" => Ok(Code::NoOp(val)),
            _ => Err("unknown code"),
        }
    }
}

impl<'a> Program {
    pub fn from(input: &Vec<String>) -> Self {
        let mut codes: Vec<Code> = Vec::new();
        for s in input {
            codes.push(Code::from_str(s).unwrap());
        }
        Self {
            pointer: 0,
            accumulator: 0,
            original: codes.clone(),
            code: codes.clone(),
            pos_count: HashMap::new(),
        }
    }

    fn step(&mut self) {
        let pos_count = self.pos_count.entry(self.pointer as usize).or_insert(0);
        *pos_count += 1;
        match self.code[self.pointer as usize] {
            Code::Acc(v) => {
                self.accumulator += v;
                self.pointer += 1;
            },
            Code::Jump(v) => {
                self.pointer += v;
            },
            _  => self.pointer += 1,
        }
    }

    pub fn run_one(&mut self) {
        loop {
            if self.finished() {
                break
            }
            if let Some(_) = self.pos_count.get(&(self.pointer as usize)) {
                // If C exists this would be the second exec stop now
                break
            }
            self.step()
        }
    }

    fn finished(&self) -> bool {
        self.pointer == (self.code.len() ) as i32
    }

    fn reset(&mut self) {
        self.pointer = 0;
        self.accumulator = 0;
        self.pos_count = HashMap::new();
        self.code = self.original.clone();
    }

    pub fn fix(&mut self) {
        for i in 0..self.original.len() {
            self.reset();
            match self.code[i] {
                Code::Jump(v) => {
                    self.code[i] = Code::NoOp(v);
                    self.run_one();
                    if self.finished() {
                        return
                    }

                },
                Code::NoOp(v) => {
                    self.code[i] = Code::Jump(v);
                    self.run_one();
                    if self.finished() {
                        return
                    }
                }
                _ => {},
            }
        }

    }

}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn code_from_str_nop() {
        match Code::from_str("nop 1") {
            Ok(v) => assert_eq!(Code::NoOp(1), v),
            Err(v) => assert_eq!("", v),
        }
    }

    #[test]
    fn code_from_str_jmp() {
        match Code::from_str("jmp 2") {
            Ok(v) => assert_eq!(Code::Jump(2), v),
            Err(v) => assert_eq!("", v),
        }
    }

    #[test]
    fn code_from_str_acc() {
        match Code::from_str("acc 2") {
            Ok(v) => assert_eq!(Code::Acc(2), v),
            Err(v) => assert_eq!("", v),
        }
    }

    #[test]
    fn code_from_str_neg() {
        match Code::from_str("acc -2") {
            Ok(v) => assert_eq!(Code::Acc(-2), v),
            Err(v) => assert_eq!("", v),
        }
    }

    #[test]
    fn check_provided_run_one() {
        let input: Vec<String> = vec!["nop +0".to_string(),
            "acc +1".to_string(),
            "jmp +4".to_string(),
            "acc +3".to_string(),
            "jmp -3".to_string(),
            "acc -99".to_string(),
            "acc +1".to_string(),
            "jmp -4".to_string(),
            "acc +6".to_string(),
        ];
        let mut prog = Program::from(&input);
        prog.run_one();
        assert_eq!(5, prog.accumulator)
    }

    #[test]
    fn check_provided_fix() {
        let input: Vec<String> = vec!["nop +0".to_string(),
            "acc +1".to_string(),
            "jmp +4".to_string(),
            "acc +3".to_string(),
            "jmp -3".to_string(),
            "acc -99".to_string(),
            "acc +1".to_string(),
            "jmp -4".to_string(),
            "acc +6".to_string(),
        ];
        let mut prog = Program::from(&input);
        prog.fix();
        assert_eq!(8, prog.accumulator)
    }
}
