use std::collections::HashMap;
use std::fs::File;
use crate::lib::{AdventDay, AdventError};

#[derive(Debug)]
struct Day {
    target: i32,
}

pub fn new() -> &'static impl AdventDay {
    Day::new()
}

impl AdventDay for Day {
    fn part1(&self, input: &mut File) -> String {
        let entries: Vec<i32>;
        match self.read_input(input) {
            Ok(v) => {entries = self.format_entries(&v);},
            Err(_) => {entries = vec![]},
        }

        match self.find_two_sum(&entries) {
            Ok(v) => format!("{}", v.0 * v.1),
            Err(AdventError::NoAnswerFound) => format!("No solution was present in dataset"),
            Err(_) => format!("Unknown Error Occured"),
        }
    }

    fn part2(&self, input: &mut File) -> String {
        let entries: Vec<i32>;
        match self.read_input(input) {
            Ok(v) => {entries = self.format_entries(&v);},
            Err(_) => {entries = vec![]},
        }

        match self.find_three_sum(&entries) {
            Ok(v) => format!("{}", v.0 * v.1 * v.2),
            Err(AdventError::NoAnswerFound) => format!("No solution was present in dataset"),
            Err(_) => format!("Unknown Error Occured"),
        }
    }
}

impl Day {

    fn format_entries(&self, input: &Vec<String>) -> Vec<i32> {
        let mut entries: Vec<i32> = Vec::new();
        for line in input {
            let entry: i32 = line.parse().unwrap();
            entries.push(entry);
        }
        entries.sort();
        entries
    }

    fn find_two_sum(&self, entries: &Vec<i32>) -> Result<(i32, i32), AdventError> {
        if entries.len() < 2 {
            Err(AdventError::NoAnswerFound)
        } else {
            let mut head = 0;
            let mut tail = entries.len() - 1;
            loop {
                if head == tail {
                    break;
                } else {
                    let total = entries[head] + entries[tail];
                    if total == self.target {
                        break;
                    } else if total < self.target {
                        head = head + 1;
                    } else {
                        tail = tail - 1;
                    }
                }
            }
            if entries[head] + entries[tail] != self.target {
                Err(AdventError::NoAnswerFound)
            } else {
                Ok((entries[head], entries[tail]))
            }
        }
    }

    fn find_three_sum(&self, entries: &Vec<i32>) -> Result<(i32, i32, i32), AdventError> {
        if entries.len() < 3 {
            Err(AdventError::NoAnswerFound)
        } else {
            // get an entry map to find the 3rd index based on our first two
            let entry_map = self.build_entry_map(&entries);

            let mut found = false;
            let mut head = 0;
            let mut tail = 1;
            let mut offset: i32;

            loop {
                offset = self.target - entries[head] - entries[tail];
                match entry_map.get(&offset) {
                    Some(_) => {
                        found = true;
                        break
                    },
                    None => {},
                }

                if offset > 0 {
                    tail = tail + 1;
                } else {
                    head = head + 1;
                    tail = head + 1;
                }

                if head == entries.len() - 1{
                    break
                }
            }

            if found {
                Ok((entries[head], entries[tail], offset))
            } else {
                Err(AdventError::NoAnswerFound)
            }
        }
    }

    fn build_entry_map(&self, entries: &Vec<i32>) -> HashMap<i32, bool> {
        let mut map: HashMap<i32, bool> = HashMap::new();
        for entry in entries {
            map.insert(*entry, true);
        }
        map
    }

    pub fn new() -> &'static Self {
        &Self{
            target: 2020,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::error::Error;


    #[test]
    fn test_build_entry_map() {
        let day = Day::new();
        let mut vec: Vec<i32> = Vec::new();
        let mut map: HashMap<i32, bool> = HashMap::new();
        for i in 1..10 {
            vec.push(i);
            map.insert(i, true);
        }

        assert_eq!(map, day.build_entry_map(&vec))
    }

    #[test]
    fn test_find_two_sum_provided() -> Result<(), Box<dyn Error>> {
        let sample: Vec<i32> = vec![1721, 979, 366, 299, 675, 1456];
        let day = Day::new();
        assert_eq!(day.find_two_sum(&sample)?, (1721, 299));
        Ok(())
    }

    #[test]
    fn test_find_two_sum_empty() {
        let sample: Vec<i32> = Vec::new();
        let day = Day::new();
        match day.find_two_sum(&sample) {
            Ok(_) => assert!(false),
            Err(AdventError::NoAnswerFound) => assert!(true),
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn test_find_two_sum_one() {
        let sample: Vec<i32> =  vec![1];
        let day = Day::new();
        match day.find_two_sum(&sample) {
            Ok(_) => assert!(false),
            Err(AdventError::NoAnswerFound) => assert!(true),
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn test_find_three_sum_provided() -> Result<(), Box<dyn Error>>{
        let sample: Vec<i32> = vec![1721, 979, 366, 299, 675, 1456];
        let day = Day::new();
        assert_eq!(day.find_three_sum(&sample)?, (979, 366, 675));
        Ok(())
    }

    #[test]
    fn test_find_three_sum_empty() {
        let sample: Vec<i32> = Vec::new();
        let day = Day::new();
        match day.find_three_sum(&sample) {
            Ok(_) => assert!(false),
            Err(AdventError::NoAnswerFound) => assert!(true),
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn test_find_three_sum_one() {
        let sample: Vec<i32> = vec![1];
        let day = Day::new();
        match day.find_three_sum(&sample) {
            Ok(_) => assert!(false),
            Err(AdventError::NoAnswerFound) => assert!(true),
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn test_find_three_sum_two() {
        let sample: Vec<i32> = vec![1,2];
        let day = Day::new();
        match day.find_three_sum(&sample) {
            Ok(_) => assert!(false),
            Err(AdventError::NoAnswerFound) => assert!(true),
            Err(_) => assert!(false),
        }
    }
}
