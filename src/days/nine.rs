use std::fs::File;
use crate::lib::AdventDay;

struct Day {}

struct XMASCrypt {
    preamble: usize,
    seq: Vec<i64>
}

pub fn new() -> &'static impl AdventDay {
    Day::new()
}

impl AdventDay for Day {
    fn part1(&self, input: &mut File) -> String {
        let xmas = XMASCrypt::from(&self.read_input(input).unwrap(), 25);
        let invalid = xmas.find_invalid();
        format!("Pos {}: Value: {}", invalid[0].0, invalid[0].1)
    }
    fn part2(&self, input: &mut File) -> String {
        let xmas = XMASCrypt::from(&self.read_input(input).unwrap(), 25);
        let exploit = xmas.find_exploit();
        format!("Exploit found: {}", exploit)
    }
}

impl<'a> Day {
    pub fn new() -> &'a Self{
        &Self{}
    }
}

impl XMASCrypt {
    pub fn from(input: &Vec<String>, preamble: usize) -> Self {
        let seq: Vec<i64> = input.into_iter().map(|x| x.parse::<i64>().unwrap()).collect();
        Self{
            seq: seq,
            preamble,
        }
    }

    pub fn find_invalid(&self) -> Vec<(usize, i64)> {
        let mut invalid: Vec<(usize, i64)> = Vec::new();
        for i in self.preamble..self.seq.len() {
            if !self.check_slice_for_target(i.checked_sub(self.preamble).unwrap(), i, self.seq[i]) {
                invalid.push((i, self.seq[i]));
            }
        }
        invalid
    }

    fn check_slice_for_target(&self, start: usize, end: usize, target: i64) -> bool {
        let mut slice: Vec<i64> = vec![0; self.preamble];
        slice.clone_from_slice(&self.seq[start..end]);
        slice.sort();
        let mut head: usize = 0;
        let mut tail = self.preamble.checked_sub(1).unwrap();
        loop {
            let result = slice[head] + slice[tail];
            if result == target {
                return true
            } else if result > target {
                tail = tail.checked_sub(1).unwrap();
            } else {
                head += 1;
            }

            if head == tail {
                break
            }
        }
        false
    }

    pub fn find_exploit(&self) -> i64  {
        let target = self.find_invalid()[0].1;
        println!("Looking for sum of: {}", target);
        let mut start: usize;
        let mut end: usize;
        let mut sum: i64;
        for i in 0..self.seq.len() {
            start = i;
            end = i;
            sum = 0;
            while sum < target {
                sum += self.seq[end];
                if sum == target {
                    let mut slice: Vec<i64> = vec![0; end.checked_sub(start).unwrap()+1];
                    slice.clone_from_slice(&self.seq[start..end+1]);
                    slice.sort();
                    return slice.first().unwrap() + slice.last().unwrap()
                }
                end += 1;
            }
        }
        0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_invalid() {
        let input: Vec<String> = vec!["35".to_string(),
            "20".to_string(),
            "15".to_string(),
            "25".to_string(),
            "47".to_string(),
            "40".to_string(),
            "62".to_string(),
            "55".to_string(),
            "65".to_string(),
            "95".to_string(),
            "102".to_string(),
            "117".to_string(),
            "150".to_string(),
            "182".to_string(),
            "127".to_string(),
            "219".to_string(),
            "299".to_string(),
            "277".to_string(),
            "309".to_string(),
            "576".to_string(),];
        let xmas = XMASCrypt::from(&input, 5);
        let mut expected: Vec<(usize, i64)> = Vec::new();
        expected.push((14, 127));
        assert_eq!(expected, xmas.find_invalid())
    }

    #[test]
    fn test_find_exploit() {
        let input: Vec<String> = vec!["35".to_string(),
            "20".to_string(),
            "15".to_string(),
            "25".to_string(),
            "47".to_string(),
            "40".to_string(),
            "62".to_string(),
            "55".to_string(),
            "65".to_string(),
            "95".to_string(),
            "102".to_string(),
            "117".to_string(),
            "150".to_string(),
            "182".to_string(),
            "127".to_string(),
            "219".to_string(),
            "299".to_string(),
            "277".to_string(),
            "309".to_string(),
            "576".to_string(),];
        let xmas = XMASCrypt::from(&input, 5);
        assert_eq!(62, xmas.find_exploit())
    }
}
