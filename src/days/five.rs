use std::fs::File;
use std::collections::HashMap;
use crate::lib::AdventDay;

#[derive(Debug)]
struct Day {
    rows: i32,
    cols: i32,
}

#[derive(Debug)]
enum Errors {
    SeatNotFound,
}

#[derive(Debug, PartialEq)]
struct Seat {
    row: i32,
    col: i32,
}

#[derive(Debug)]
struct Range {
    min: i32,
    max: i32,
}

pub fn new() -> &'static impl AdventDay {
    Day::new()
}

impl AdventDay for Day {
    fn part1(&self, input: &mut File) -> String {
        let mut seats = self.fmt_input(&self.read_input(input).unwrap());
        seats.sort_by_cached_key(|x| x.get_id());
        format!("{:?}", seats.last().unwrap().get_id())
    }
    fn part2(&self, input: &mut File) -> String {
        let seats = self.fmt_input(&self.read_input(input).unwrap());
        match self.find_neighbor_id_seats(&seats) {
            Ok(v) => format!("Seat Found! ID: {}", v.get_id()),
            Err(_) => format!("Seat not found"),
        }
    }
}

impl Day {
    pub fn new() -> &'static Self {
        &Self{rows: 127, cols: 7}
    }

    fn fmt_input(&self, input: &Vec<String>) -> Vec<Seat> {
        input.into_iter()
            .map(|x| Seat::from(x.to_string()).unwrap())
            .collect()
    }

    fn build_seat_map(&self, seats: &Vec<Seat>) -> HashMap<(i32,i32), i32> {
        let mut seat_map: HashMap<(i32,i32), i32> = HashMap::new();
        for s in seats {
            seat_map.insert((s.row, s.col), s.get_id());
        }
        seat_map
    }

    fn find_neighbor_id_seats(&self, seats: &Vec<Seat>) -> Result<Seat, Errors> {
        let mut seat: Result<Seat, Errors> = Err(Errors::SeatNotFound);
        let seat_map = self.build_seat_map(seats);
        for s in self.find_missing_seats(seats) {
            let neighbors: usize  = seat_map.values()
                .filter(|x| **x == s.get_id() + 1 || **x == s.get_id() - 1)
                .collect::<Vec<&i32>>()
                .len();
            if neighbors == 2 {
                seat = Ok(s);
                break
            }
        }
        seat
    }

    fn find_missing_seats(&self, seats: &Vec<Seat>) -> Vec<Seat>{
        let mut missing: Vec<Seat> = Vec::new();
        let seat_map = self.build_seat_map(seats);

        // println!("{:?}", seat_map);
        for row in 0..127 {
            for col in 0..8 {
                if ! seat_map.contains_key(&(row, col)) {
                    missing.push(Seat::new(row, col));
                }
            }
        }
        missing
    }
}

impl Seat {
    pub fn new(row: i32, col: i32) -> Seat {
        Seat{row: row, col: col,}
    }

    pub fn from(s: String) -> Result<Seat, Errors> {
        let mut rows = Range::new(0, 127);
        let mut cols = Range::new(0, 7);
        for c in s.chars() {
            match c {
                'F' =>  rows.lower().unwrap(),
                'B' => rows.upper().unwrap(),
                'R' => cols.upper().unwrap(),
                'L' => cols.lower().unwrap(),
                _ => {},
            }
        }

        match (rows.get(), cols.get()) {
            (Ok(row), Ok(col)) => Ok(Seat::new(row, col)),
            _ => Err(Errors::SeatNotFound)
        }
    }

    pub fn get_id(&self) -> i32 {
        (self.row * 8) + self.col
    }
}

impl Range {
    pub fn new(min: i32, max: i32) -> Range {
        Range{min: min, max: max}
    }

    pub fn upper(&mut self) -> Result<(), ()> {
         let range = self.max - self.min + 1;
         let step = range/2;
         if self.min + step > self.max {
             Err(())
         } else {
             self.min += step;
             Ok(())
         }
    }

    pub fn lower(&mut self) -> Result<(), ()> {
         let range = self.max - self.min + 1;
         let step = range/2;
         if self.max - step < self.min {
             Err(())
         } else {
             self.max -= step;
             Ok(())
         }
    }

    pub fn get(&self) -> Result<i32, ()> {
        if self.min == self.max {
            Ok(self.max)
        } else {
            Err(())
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_range_upper() {
        let mut range = Range::new(0, 7);
        range.upper().unwrap();
        assert_eq!(range.min, 4)
    }

    #[test]
    fn test_range_lower() {
        let mut range = Range::new(0, 7);
        range.lower().unwrap();
        assert_eq!(range.max, 3)
    }

    #[test]
    fn test_range_lower_close() {
        let mut range = Range::new(0, 1);
        range.lower().unwrap();
        assert_eq!(range.max, 0)
    }

    #[test]
    fn test_range_upper_close() {
        let mut range = Range::new(0, 1);
        range.upper().unwrap();
        assert_eq!(range.min, 1)
    }

    #[test]
    fn seat_from_provided_1() {
        let seat = Seat::from("FBFBBFFRLR".to_string());
        match seat {
            Ok(s) => assert_eq!(s, Seat::new(44, 5)),
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn seat_from_provided_2() {
        let seat = Seat::from("BFFFBBFRRR".to_string());
        match seat {
            Ok(s) => assert_eq!(s, Seat::new(70, 7)),
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn seat_from_provided_3() {
        let seat = Seat::from("FFFBBBFRRR".to_string());
        match seat {
            Ok(s) => assert_eq!(s, Seat::new(14, 7)),
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn seat_from_provided_4() {
        let seat = Seat::from("BBFFBBFRLL".to_string());
        match seat {
            Ok(s) => assert_eq!(s, Seat::new(102, 4)),
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn seat_id_provided_1() {
        let seat = Seat::from("FBFBBFFRLR".to_string());
        match seat {
            Ok(s) => assert_eq!(s.get_id(), 357),
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn seat_id_provided_2() {
        let seat = Seat::from("BFFFBBFRRR".to_string());
        match seat {
            Ok(s) => assert_eq!(s.get_id(), 567),
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn seat_id_provided_3() {
        let seat = Seat::from("FFFBBBFRRR".to_string());
        match seat {
            Ok(s) => assert_eq!(s.get_id(), 119),
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn seat_id_provided_4() {
        let seat = Seat::from("BBFFBBFRLL".to_string());
        match seat {
            Ok(s) => assert_eq!(s.get_id(), 820),
            Err(_) => assert!(false)
        }
    }
}
