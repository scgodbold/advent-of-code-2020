use std::fs::File;
use std::collections::HashMap;
use std::collections::hash_map::Keys;
use crate::lib::AdventDay;

#[derive(Debug)]
struct Day {
}

#[derive(Debug, PartialEq)]
struct Group {
    size: usize,
    answers: HashMap<char, usize>,
}

pub fn new() -> &'static impl AdventDay {
    Day::new()
}

impl AdventDay for Day {
    fn part1(&self, input: &mut File) -> String {
        let answer_count: usize = self.fmt_input(&self.read_input(input).unwrap())
            .into_iter()
            .map(|x| x.unique_answers().len())
            .sum();
        format!("{:?}", answer_count)
    }
    fn part2(&self, input: &mut File) -> String {
        let answer_count: usize = self.fmt_input(&self.read_input(input).unwrap())
            .into_iter()
            .map(|x| x.concensus_answers().len())
            .sum();
        format!("{}", answer_count)
    }
}

impl Day {
    pub fn new() -> &'static Self {
        &Self{} }

    fn fmt_input(&self, input: &Vec<String>) -> Vec<Group> {
        let mut groups: Vec<Group> = Vec::new();
        let mut glob: Vec<String> = Vec::new();
        for line in input {
            if line == "" {
                groups.push(Group::from(glob));
                glob = Vec::new();
            } else {
                glob.push(line.to_string());
            }
        }
        groups.push(Group::from(glob));
        groups
    }
}

impl Group {
    pub fn from(input: Vec<String>) -> Group {
        let mut group = Group{answers: HashMap::new(), size: input.len()};
        for c in input.concat().chars() {
            if let Some(v) = group.answers.get_mut(&c) {
                *v += 1;
            } else {
                group.answers.insert(c, 1);
            }
        }
        group
    }

    pub fn unique_answers(&self) -> Keys<char, usize> {
        self.answers.keys().into_iter()
    }

    pub fn concensus_answers(&self) -> Vec<&char> {
        self.answers.iter()
            .filter(|x| *x.1 == self.size)
            .map(|x| x.0)
            .collect::<Vec<&char>>()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_group() {
        let input: Vec<String> = vec!["ab".to_string(), "ac".to_string()];
        let group = Group::from(input);
        let mut expected: HashMap<char, usize> = HashMap::new();
        expected.insert('a', 2);
        expected.insert('b', 1);
        expected.insert('c', 1);
        assert_eq!(group, Group{answers: expected, size: 2})
    }

    #[test]
    fn test_unique_answers() {
        let input: Vec<String> = vec!["ab".to_string(), "ac".to_string()];
        let group = Group::from(input);
        let expected: Vec<&char> = vec![&'a', &'b', &'c'];
        let mut val = group.unique_answers().collect::<Vec<&char>>();
        val.sort();
        assert_eq!(val, expected);
    }

    #[test]
    fn test_concensus_answers() {
        let input: Vec<String> = vec!["ab".to_string(), "ac".to_string()];
        let group = Group::from(input);
        let expected: Vec<&char> = vec![&'a'];
        assert_eq!(group.concensus_answers(), expected);
    }
}
