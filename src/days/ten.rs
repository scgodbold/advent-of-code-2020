use std::fs::File;
use std::collections::HashMap;
use crate::lib::AdventDay;


struct Day {}

pub fn new() -> &'static impl AdventDay {
    Day::new()
}

impl AdventDay for Day {
    fn part1(&self, input: &mut File) -> String {
        let adapters = self.fmt_input(&self.read_input(input).unwrap());
        let jolt_count = self.count_jolt_diff(&adapters);
        let three = jolt_count.get(&3).unwrap();
        let one = jolt_count.get(&1).unwrap();
        format!("{}", three * one)
    }
    fn part2(&self, input: &mut File) -> String {
        let adapters = self.fmt_input(&self.read_input(input).unwrap());
        let valid_paths = self.permutate(&adapters);
        format!("{}", valid_paths)
    }
}

impl<'a> Day {
    pub fn new() -> &'a Self{
        &Self{}
    }

    fn fmt_input(&self, input: &Vec<String>) -> Vec<i32> {
        let mut resp: Vec<i32> = input.into_iter()
            .map(|x| x.parse::<i32>().unwrap())
            .collect();
        resp.sort();
        resp
    }

    fn count_jolt_diff(&self, chain: &Vec<i32>) -> HashMap<i32, i32> {
        let mut counts: HashMap<i32, i32> = HashMap::new();
        for i in 0..chain.len() {
            if i + 1 == chain.len() {
                // We always are +3 at the end
                let count = counts.entry(3).or_insert(0);
                *count += 1;
                break
            }
            let diff: i32;
            if i == 0 {
                let first_diff = chain[i];
                let count = counts.entry(first_diff).or_insert(0);
                *count += 1;
            }
            diff = chain[i+1] - chain[i];
            let count = counts.entry(diff).or_insert(0);
            *count += 1
        }
        counts
    }

    fn valid_steps(&self, hashstack: &Vec<i32>, needle: i32) -> usize {
        let mut steps = 0;
        for i in 1..=3 {
            if hashstack.contains(&(needle+i)) {
                steps += 1;
            }
        }
        steps
    }


    fn permutate(&self, chain: &Vec<i32>) -> i64 {
        let mut forks: Vec<i64> = vec![0; chain.len()];

        // Calc first step, this case is special
        for zstep in 0..self.valid_steps(chain, 0) {
            forks[zstep] = 1
        }

        for i in 0..chain.len() {
            let steps = self.valid_steps(chain, chain[i]);
            for j in 1..=steps {
                forks[i+j] += forks[i];
            }
        }
        *forks.last().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_provided_one() {
        let mut input = vec![16,10,15,5,1,11,7,19,6,12,4];
        let mut expected: HashMap<i32, i32> = HashMap::new();
        expected.insert(1, 7);
        expected.insert(3, 5);
        input.sort();
        let day = Day::new();
        assert_eq!(expected, day.count_jolt_diff(&input))

    }

    #[test]
    fn test_provided_two() {
        let mut input = vec![28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3,];
        let mut expected: HashMap<i32, i32> = HashMap::new();
        expected.insert(1, 22);
        expected.insert(3, 10);
        input.sort();
        let day = Day::new();
        assert_eq!(expected, day.count_jolt_diff(&input))

    }

    #[test]
    fn test_provided_two_one() {
        let mut input = vec![16,10,15,5,1,11,7,19,6,12,4];
        input.sort();
        let day = Day::new();
        assert_eq!(8, day.permutate(&input))

    }

    #[test]
    fn test_provided_two_mid() {
        let mut input = vec![17, 6, 10, 5, 13, 7, 1, 4, 12, 11, 14];
        input.sort();
        let day = Day::new();
        assert_eq!(28, day.permutate(&input))
    }

    #[test]
    fn test_provided_two_two() {
        let mut input = vec![28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3,];
        input.sort();
        let day = Day::new();
        assert_eq!(19208, day.permutate(&input))
    }
}
