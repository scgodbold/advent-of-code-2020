use std::fs::File;
use std::iter::FromIterator;
use parse_display::{Display, FromStr};
use crate::lib::AdventDay;

#[derive(Debug)]
struct Day {
}

#[derive(Display, FromStr, Debug, PartialEq)]
#[display("{min}-{max} {target}: {password}")]
struct PasswdEntry {
    min: i32,
    max: i32,
    target: char,
    password: String,
}

pub fn new() -> &'static impl AdventDay {
    Day::new()
}

impl AdventDay for Day {
    fn part1(&self, input: &mut File) -> String {
        let passwds: Vec<PasswdEntry>;
        match self.read_input(input) {
            Ok(v) => {passwds =self.fmt_input(&v);},
            Err(_) => {passwds = vec![]},
        }
        format!("{}", self.count_valid_occurance(&passwds))
    }
    fn part2(&self, input: &mut File) -> String {
        let passwds: Vec<PasswdEntry>;
        match self.read_input(input) {
            Ok(v) => {passwds =self.fmt_input(&v);},
            Err(_) => {passwds = vec![]},
        }
        format!("{}", self.count_valid_position(&passwds))
    }
}

impl Day {
    pub fn new() -> &'static Self {
        &Self{}
    }

    fn fmt_input(&self, inputs: &Vec<String>) -> Vec<PasswdEntry> {
        let mut passwds: Vec<PasswdEntry> = vec![];
        for line in inputs {
            let passwd = line.parse().unwrap();
            passwds.push(passwd);
        }
        passwds
    }

    fn count_valid_occurance(&self, passwds: &Vec<PasswdEntry>) -> usize {
        let valid_passwds: Vec<&PasswdEntry> = Vec::from_iter(passwds.iter()
                                                             .filter(|x| x.valid_by_occurence()));
        return valid_passwds.len()
    }

    fn count_valid_position(&self, passwds: &Vec<PasswdEntry>) -> usize {
        let valid_passwds: Vec<&PasswdEntry> = Vec::from_iter(passwds.iter()
                                                             .filter(|x| x.valid_by_position()));
        return valid_passwds.len()
    }
}

impl PasswdEntry {
    pub fn valid_by_occurence(&self) -> bool {
        let mut target_count = 0;
        for c in self.password.chars() {
            if c == self.target {
                target_count = target_count + 1;
            }
        }

        if target_count >= self.min && target_count <= self.max {
            return true
        }
        return false
    }

    pub fn valid_by_position(&self) -> bool {
        let idx1 = self.password.as_bytes()[(self.min - 1) as usize] as char;
        let idx2 = self.password.as_bytes()[(self.max - 1) as usize] as char;
        let mut result = false;
        if idx1 == self.target && idx2 != self.target {
            result = true;
        } else if idx1 != self.target && idx2 == self.target {
            result = true;
        }
        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fmt_input() {
        let sample: Vec<String> = vec![String::from("1-2 a: abc")];
        let expected: Vec<PasswdEntry> = vec![PasswdEntry{min: 1,
                                                          max: 2,
                                                          target: 'a',
                                                          password: String::from("abc")}];
        let day = Day::new();
        assert_eq!(day.fmt_input(&sample), expected)
    }

    #[test]
    fn test_count_valid_occurance() {
        let sample: Vec<PasswdEntry> = vec![PasswdEntry{min: 1, max: 3, target: 'a',
                                                        password: String::from("abcde")},
                                            PasswdEntry{min: 1, max: 3, target: 'b',
                                                        password: String::from("cdefg")},
                                            PasswdEntry{min: 2, max: 9, target: 'c',
                                                        password: String::from("ccccccccc")},];
        let day = Day::new();
        assert_eq!(day.count_valid_occurance(&sample), 2)
    }

    #[test]
    fn test_count_valid_position() {
        let sample: Vec<PasswdEntry> = vec![PasswdEntry{min: 1, max: 3, target: 'a',
                                                        password: String::from("abcde")},
                                            PasswdEntry{min: 1, max: 3, target: 'b',
                                                        password: String::from("cdefg")},
                                            PasswdEntry{min: 2, max: 9, target: 'c',
                                                        password: String::from("ccccccccc")},];
        let day = Day::new();
        assert_eq!(day.count_valid_position(&sample), 1)
    }

    #[test]
    fn test_passwdentry_valid_count_good () {
        // Good Entry
        assert!("1-3 a: aba".parse::<PasswdEntry>().unwrap().valid_by_occurence());
    }

    #[test]
    fn test_passwdentry_valid_count_bad () {
        // Bad Entry
        assert!(!"4-8 a: aba".parse::<PasswdEntry>().unwrap().valid_by_occurence());
    }

    #[test]
    fn test_passwdentry_valid_count_minmax_match () {
        // Min == Max
        assert!("1-1 a: abc".parse::<PasswdEntry>().unwrap().valid_by_occurence());
    }

    #[test]
    fn test_passwdentry_valid_count_zero () {
        // Min & Max is 0
        assert!("0-0 g: abc".parse::<PasswdEntry>().unwrap().valid_by_occurence());
    }

    #[test]
    fn test_passwdentry_valid_position_two_match () {
        // Bad Entry
        assert!(!"1-3 a: aba".parse::<PasswdEntry>().unwrap().valid_by_position());
    }

    #[test]
    fn test_passwdentry_valid_position_single_match () {
        // Good Entry
        assert!("1-2 a: aba".parse::<PasswdEntry>().unwrap().valid_by_position());
    }

    #[test]
    fn test_passwdentry_valid_position_zero_match () {
        // Bad Entry No Match
        assert!(!"1-2 g: aba".parse::<PasswdEntry>().unwrap().valid_by_position());
    }
}

