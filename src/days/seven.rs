use std::fs::File;
use std::collections::HashMap;
use crate::lib::AdventDay;

struct Day {
}

#[derive(Debug, PartialEq)]
struct Bag {
    name: String,
    contents: HashMap<String, i32>,
}


pub fn new() -> &'static impl AdventDay {
    Day::new()
}

impl AdventDay for Day {
    fn part1(&self, input: &mut File) -> String {
        let bags = self.fmt_input(&self.read_input(input).unwrap());
        let matches = self.find_contains(&bags, "shiny gold".to_string());
        format!("{}", matches.len())
    }
    fn part2(&self, input: &mut File) -> String {
        let bags = self.fmt_input(&self.read_input(input).unwrap());
        let total_bags = self.sum_bags(&bags, "shiny gold".to_string());
        format!("{}", total_bags)
    }
}

impl<'a> Day {
    pub fn new() -> &'a Self {
        &Self{} }

    fn fmt_input(&self, input: &Vec<String>) -> HashMap<String, Bag> {
        let mut bag_map: HashMap<String, Bag> = HashMap::new();
        for l in input {
            let bag = Bag::from(l.to_string());
            bag_map.insert(bag.name.clone(), bag);
        }
        bag_map
    }

    fn find_contains(&self, bags: &'a HashMap<String, Bag>, target: String) -> Vec<&'a String> {
        let mut valid: Vec<&String> = Vec::new();
        for bag in bags.keys() {
            let mut contents: Vec<&String> = bags.get(bag)
                .unwrap()
                .contents
                .keys()
                .collect();
            loop {
                if let Some(c) = contents.pop() {
                    if **c == target  || valid.contains(&c) {
                        // Either it is the target or we know it contains the target already
                        valid.push(bag);
                        break
                    } else {
                        let bag_ref = bags.get(c).unwrap();
                        let mut found = false;
                        for contains in bag_ref.contents.keys() {
                            if valid.contains(&contains) {
                                found = true;
                                valid.push(bag);
                                break
                            } else if ! contents.contains(&contains) {
                                contents.push(contains)
                            }
                        }
                        if found {
                            break
                        }
                    }
                } else {
                    break
                }
            }
        }
        valid
    }

    fn sum_bags(&self, bags: &HashMap<String, Bag>, target: String) -> i32 {
        let mut sum = bags.get(&target).unwrap().num_bags();
        for (name, num) in bags.get(&target).unwrap().contents.iter() {
            sum += self.sum_bags(bags, name.to_string()) * num;
        }
        sum
    }
}

impl Bag {
    pub fn from(input: String) -> Self {
        let mut defition = input.split("contain");
        let name = defition.next()
            .unwrap()
            .replace("bags", "")
            .replace("bag", "");
        let name = name.trim();
        let mut content: HashMap<String, i32> = HashMap::new();
        for s in defition.next().unwrap().split(',') {
            if s.trim() == "no other bags." {
                continue
            }
            let mut content_split = s.trim().split(" ");
            let count: i32 = content_split.next().unwrap().parse().unwrap();
            let name = s.trim()
                .trim_start_matches(&count.to_string())
                .replace(".", "")
                .trim()
                .replace("bags", "")
                .replace("bag", "");
            let name = name.trim();
            content.insert(name.to_string(), count);
        }
        Self{
            name: name.to_string(),
            contents: content,
        }
    }

    pub fn num_bags(&self) -> i32 {
        self.contents.values().sum()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bag_from_str_no_bags() {
        let bag = Bag::from("faded blue bags contain no other bags.".to_string());
        let expected = Bag{name: "faded blue".to_string(), contents: HashMap::new()};
        assert_eq!(bag, expected)
    }

    #[test]
    fn test_bag_from_str_one() {
        let bag = Bag::from("bright white bags contain 1 shiny gold bag.".to_string());
        let mut content_map: HashMap<String, i32> = HashMap::new();
        content_map.insert("shiny gold".to_string(), 1);
        let expected = Bag{name: "bright white".to_string(), contents: content_map};
        assert_eq!(bag, expected)
    }

    #[test]
    fn test_bag_from_str_n() {
        let bag = Bag::from("vibrant plum bags contain 5 faded blue bags, 6 dotted olive bags.".to_string());
        let mut content_map: HashMap<String, i32> = HashMap::new();
        content_map.insert("faded blue".to_string(), 5);
        content_map.insert("dotted olive".to_string(), 6);
        let expected = Bag{name: "vibrant plum".to_string(), contents: content_map};
        assert_eq!(bag, expected)
    }

    #[test]
    fn test_num_bags_zero() {
        let bag = Bag::from("faded blue bags contain no other bags.".to_string());
        assert_eq!(bag.num_bags(), 0)
    }

    #[test]
    fn test_num_bags() {
        let bag = Bag::from("vibrant plum bags contain 5 faded blue bags, 6 dotted olive bags.".to_string());
        assert_eq!(bag.num_bags(), 11)
    }

    #[test]
    fn sum_bags_one() {
        let mut input: Vec<String> = Vec::new();
        input.push("shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.".to_string());
        input.push("faded blue bags contain no other bags.".to_string());
        input.push("dotted olive bags contain no other bags.".to_string());
        input.push("vibrant plum bags contain 5 faded blue bags, 6 dotted olive bags.".to_string());
        input.push("dark olive bags contain 3 faded blue bags, 4 dotted olive bags.".to_string());
        let day = Day::new();
        let bag_map = day.fmt_input(&input);
        assert_eq!(0, day.sum_bags(&bag_map, "dotted olive".to_string()))
    }

    #[test]
    fn sum_bags_two() {
        let mut input: Vec<String> = Vec::new();
        input.push("shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.".to_string());
        input.push("faded blue bags contain no other bags.".to_string());
        input.push("dotted olive bags contain no other bags.".to_string());
        input.push("vibrant plum bags contain 5 faded blue bags, 6 dotted olive bags.".to_string());
        input.push("dark olive bags contain 3 faded blue bags, 4 dotted olive bags.".to_string());
        let day = Day::new();
        let bag_map = day.fmt_input(&input);
        assert_eq!(7, day.sum_bags(&bag_map, "dark olive".to_string()))
    }

    #[test]
    fn sum_bags_two_2() {
        let mut input: Vec<String> = Vec::new();
        input.push("shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.".to_string());
        input.push("faded blue bags contain no other bags.".to_string());
        input.push("dotted olive bags contain no other bags.".to_string());
        input.push("vibrant plum bags contain 5 faded blue bags, 6 dotted olive bags.".to_string());
        input.push("dark olive bags contain 3 faded blue bags, 4 dotted olive bags.".to_string());
        let day = Day::new();
        let bag_map = day.fmt_input(&input);
        assert_eq!(11, day.sum_bags(&bag_map, "vibrant plum".to_string()))
    }

    #[test]
    fn sum_bags_provided_one() {
        let mut input: Vec<String> = Vec::new();
        input.push("shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.".to_string());
        input.push("faded blue bags contain no other bags.".to_string());
        input.push("dotted olive bags contain no other bags.".to_string());
        input.push("vibrant plum bags contain 5 faded blue bags, 6 dotted olive bags.".to_string());
        input.push("dark olive bags contain 3 faded blue bags, 4 dotted olive bags.".to_string());
        let day = Day::new();
        let bag_map = day.fmt_input(&input);
        assert_eq!(32, day.sum_bags(&bag_map, "shiny gold".to_string()))
    }
}
