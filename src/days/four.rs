use std::fs::File;
use regex::Regex;
use crate::lib::AdventDay;

#[derive(Debug)]
struct Day {
}

#[derive(Debug)]
struct Passport {
    birth_year: Option<String>,
    issue_year: Option<String>,
    expiration_year: Option<String>,
    height: Option<String>,
    hair_color: Option<String>,
    eye_color: Option<String>,
    id: Option<String>,
    country_id: Option<String>,
}

pub fn new() -> &'static impl AdventDay {
    Day::new()
}

impl AdventDay for Day {
    fn part1(&self, input: &mut File) -> String {
        let passports = self.fmt_input(&self.read_input(input).unwrap())
            .into_iter()
            .filter(|x| x.valid())
            .collect::<Vec<Passport>>()
            .len();
        format!("{}", passports)
    }
    fn part2(&self, input: &mut File) -> String {
        let passports = self.fmt_input(&self.read_input(input).unwrap())
            .into_iter()
            .filter(|x| x.valid_strict())
            .collect::<Vec<Passport>>()
            .len();
        format!("{}", passports)
    }
}

impl Day {
    pub fn new() -> &'static Self {
        &Self{}
    }

    fn fmt_input(&self, input: &Vec<String>) -> Vec<Passport> {
        let mut result: Vec<Passport> = Vec::new();
        let mut glob: Vec<String> = Vec::new();
        for line in input {
            if line == "" {
                result.push(Passport::from(glob.join(" ")));
                glob = Vec::new();
            } else {
                glob.push(line.to_string());
            }
        }

        // Push last one onto the stack
        result.push(Passport::from(glob.join(" ")));
        result
    }
}

impl Passport {
    pub fn empty() -> Passport{
        Passport{birth_year: None,
                 issue_year: None,
                 expiration_year: None,
                 height: None,
                 hair_color: None,
                 eye_color: None,
                 id: None,
                 country_id: None,}
    }

    pub fn from(input: String) -> Passport {
        let mut passport = Passport::empty();

        for token in input.split(" ") {
            let mut field_split = token.split(":");
            let field = field_split.next().unwrap();
            let value = field_split.next().unwrap();
            match field {
                "byr" => passport.birth_year = Some(value.to_string()),
                "iyr" => passport.issue_year = Some(value.to_string()),
                "eyr" => passport.expiration_year = Some(value.to_string()),
                "hgt" => passport.height = Some(value.to_string()),
                "hcl" => passport.hair_color = Some(value.to_string()),
                "ecl" => passport.eye_color = Some(value.to_string()),
                "pid" => passport.id = Some(value.to_string()),
                "cid" => passport.country_id = Some(value.to_string()),
                _ => {},
            }
        }
        passport
    }

    fn valid_byr(&self) -> bool {
        let year: i32;
        match &self.birth_year {
            Some(v) => year = v.parse().unwrap(),
            None => year = 0,
        }
        year >= 1920 && year <= 2002
    }

    fn valid_iyr(&self) -> bool {
        let year: i32;
        match &self.issue_year {
            Some(v) => year = v.parse().unwrap(),
            None => year = 0,
        }
        year >= 2010 && year <= 2020
    }

    fn valid_eyr(&self) -> bool {
        let year: i32;
        match &self.expiration_year {
            Some(v) => year = v.parse().unwrap(),
            None => year = 0,
        }
        year >= 2020 && year <= 2030
    }

    // Not needed but it felt left out without it
    fn valid_cid(&self) -> bool {
        true
    }

    fn valid_pid(&self) -> bool {
        match &self.id {
            Some(v) => {
                let re = Regex::new(r"^\d{9}$").unwrap();
                re.is_match(v)
            },
            None => false,
        }
    }

    fn valid_hgt(&self) -> bool {
        match &self.height {
            Some(v) => {
                let metric = Regex::new(r"^(\d{3})cm$").unwrap();
                let imperial = Regex::new(r"^(\d{2})in$").unwrap();
                if metric.is_match(v) {
                    let val: i32 = metric.captures(v).unwrap()[1].parse().unwrap();
                    val >= 150 && val <= 193
                } else if imperial.is_match(v) {
                    let val: i32 = imperial.captures(v).unwrap()[1].parse().unwrap();
                    val >= 59 && val <= 76
                } else {
                    false
                }
            },
            None => false
        }
    }

    fn valid_hcl(&self) -> bool {
        match &self.hair_color {
            Some(v) => {
                let re = Regex::new(r"^#[0-9a-f]{6}").unwrap();
                re.is_match(v)
            }
            None => false,
        }
    }

    fn valid_ecl(&self) -> bool {
        match &self.eye_color {
            Some(v) => {
                let re = Regex::new(r"amb|blu|brn|gry|grn|hzl|oth").unwrap();
                re.is_match(v)
            },
            None => false,
        }
    }

    pub fn valid_strict(&self) -> bool {
        self.valid_byr() && self.valid_iyr() && self.valid_eyr() && self.valid_cid() && self.valid_pid() && self.valid_hgt() && self.valid_hcl() && self.valid_ecl()
    }

    pub fn valid(&self) -> bool {
        if self.birth_year == None || self.issue_year == None || self.expiration_year == None || self.height == None || self.hair_color == None || self.eye_color == None || self.eye_color == None || self.id == None {
            false
        } else {
            true
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_passport_byr() {
        let passport = Passport::from("byr:1".to_string());
        assert_eq!(passport.birth_year, Some("1".to_string()))
    }
    #[test]
    fn test_passport_eyr() {
        let passport = Passport::from("eyr:1".to_string());
        assert_eq!(passport.expiration_year, Some("1".to_string()))
    }

    #[test]
    fn test_passport_iyr() {
        let passport = Passport::from("iyr:1".to_string());
        assert_eq!(passport.issue_year, Some("1".to_string()))
    }
    #[test]
    fn test_passport_hgt() {
        let passport = Passport::from("hgt:1".to_string());
        assert_eq!(passport.height, Some(String::from("1")))
    }
    #[test]
    fn test_passport_ecl() {
        let passport = Passport::from("ecl:1".to_string());
        assert_eq!(passport.eye_color, Some(String::from("1")))
    }
    #[test]
    fn test_passport_hcl() {
        let passport = Passport::from("hcl:1".to_string());
        assert_eq!(passport.hair_color, Some(String::from("1")))
    }
    #[test]
    fn test_passport_id() {
        let passport = Passport::from("pid:1".to_string());
        assert_eq!(passport.id, Some(String::from("1")))
    }
    #[test]
    fn test_passport_cid() {
        let passport = Passport::from("cid:1".to_string());
        assert_eq!(passport.country_id, Some("1".to_string()));
    }

    #[test]
    fn test_passport_valid_byr() {
        let passport = Passport::from("eyr:1 iyr:1 hgt:1 ecl:1 hcl:1 pid:1 cid:1".to_string());
        assert!(!passport.valid())
    }
    #[test]
    fn test_passport_valid_iyr() {
        let passport = Passport::from("eyr:1 byr:1 hgt:1 ecl:1 hcl:1 pid:1 cid:1".to_string());
        assert!(!passport.valid())
    }
    #[test]
    fn test_passport_valid_eyr() {
        let passport = Passport::from("iyr:1 byr:1 hgt:1 ecl:1 hcl:1 pid:1 cid:1".to_string());
        assert!(!passport.valid())
    }
    #[test]
    fn test_passport_valid_hgt() {
        let passport = Passport::from("iyr:1 byr:1 eyr:1 ecl:1 hcl:1 pid:1 cid:1".to_string());
        assert!(!passport.valid())
    }
    #[test]
    fn test_passport_valid_hcl() {
        let passport = Passport::from("iyr:1 byr:1 eyr:1 ecl:1 hgt:1 pid:1 cid:1".to_string());
        assert!(!passport.valid())
    }
    #[test]
    fn test_passport_valid_pid() {
        let passport = Passport::from("iyr:1 byr:1 eyr:1 ecl:1 hgt:1 hcl:1 cid:1".to_string());
        assert!(!passport.valid())
    }
    #[test]
    fn test_passport_valid_cid() {
        let passport = Passport::from("iyr:1 byr:1 eyr:1 ecl:1 hgt:1 hcl:1 pid:1".to_string());
        assert!(passport.valid())
    }

    #[test]
    fn test_passport_valid_provided_1() {
        let passport = Passport::from("ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm".to_string());
        assert!(passport.valid())
    }

    #[test]
    fn test_passport_valid_provided_2() {
        let passport = Passport::from("iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929".to_string());
        assert!(!passport.valid())
    }

    #[test]
    fn test_passport_valid_provided_3() {
        let passport = Passport::from("hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm".to_string());
        assert!(passport.valid())
    }

    #[test]
    fn test_passport_valid_provided_4() {
        let passport = Passport::from("hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in".to_string());
        assert!(!passport.valid())
    }

    #[test]
    fn test_fmt_input() {
        let input = vec!["hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm".to_string(),
                         "".to_string(),
                         "hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm".to_string(),];

        let day = Day::new();
        assert_eq!(2, day.fmt_input(&input).len())
    }

    #[test]
    fn test_valid_byr_min() {
        let p = Passport::from("byr:1920".to_string());
        assert!(p.valid_byr())
    }

    #[test]
    fn test_valid_byr_max() {
        let p = Passport::from("byr:2002".to_string());
        assert!(p.valid_byr())
    }

    #[test]
    fn test_valid_byr_to_large() {
        let p = Passport::from("byr:2003".to_string());
        assert!(!p.valid_byr())
    }

    #[test]
    fn test_valid_byr_to_small() {
        let p = Passport::from("byr:1919".to_string());
        assert!(!p.valid_byr())
    }
}
