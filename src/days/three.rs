use std::fs::File;
use crate::lib::{AdventDay, AdventError};

#[derive(Debug)]
struct Day {
}

#[derive(Clone, Debug, Copy)]
enum Obstacle {
    Empty,
    Tree,
}

#[derive(Debug, PartialEq)]
struct Point {
    x: usize,
    y: usize,
    max_x: usize,
    max_y: usize,
}

#[derive(Debug)]
struct Map {
    row_max: usize,
    col_max: usize,
    map: Vec<Vec<Obstacle>>,
}

pub fn new() -> &'static impl AdventDay {
    Day::new()
}

impl AdventDay for Day {
    fn part1(&self, input: &mut File) -> String {
        let slope = Point::new(3, 1, 0, 0);
        let map: Map;
        if let Ok(v) = self.read_input(input) {
            map = Map::from(&v);
            match map.solve(&slope) {
                Ok(v) => format!("{}", v),
                Err(v) => format!("{}", v),
            }
        } else {

            format!("{}", AdventError::InputNotFound)
        }
    }
    fn part2(&self, input: &mut File) -> String {
        if let Ok(v) = self.read_input(input) {
            let slopes: Vec<Point> = vec![Point::new(1,1,0,0),
                                          Point::new(3,1,0,0),
                                          Point::new(5,1,0,0),
                                          Point::new(7,1,0,0),
                                          Point::new(1,2,0,0),];
            let mut tree_product: i64 = 1;
            let map = Map::from(&v);
            let mut err = false;
            for slope in slopes {
                match map.solve(&slope) {
                    Ok(trees) => tree_product *= trees as i64,
                    Err(_) => {err = true; break;},
                }
            }

            if err {
                format!("{}", AdventError::NoAnswerFound)
            } else {
                format!("{}", tree_product)
            }

        } else {
            format!("{}", AdventError::InputNotFound)
        }
    }
}

impl Day {
    pub fn new() -> &'static Self {
        &Self{}
    }
}

impl Map {
    fn from(input: &Vec<String>) -> Self {
        let mut m: Vec<Vec<Obstacle>> = Vec::new();
        for line in input {
            let mut row: Vec<Obstacle> = Vec::new();
            for c in line.chars() {
                if c == '#' {
                    row.push(Obstacle::Tree);
                } else  {
                    row.push(Obstacle::Empty);
                }
            }
            m.push(row)
        }

        let r_max = m[0].len() - 1;
        let c_max = m.len() - 1;

        Self{
            row_max: r_max,
            col_max: c_max,
            map: m,
        }
    }

    fn get(&self, pos: &Point) -> Obstacle {
        self.map[pos.y][pos.x]
    }

    pub fn solve(&self, slope: &Point) -> Result<i32, AdventError> {
        if slope.x == 0 || slope.y == 0 {
            Err(AdventError::NoAnswerFound)
        } else {
            let mut pos = Point::new(0, 0, self.row_max, self.col_max);
            let mut trees = 0;
            loop {
                match pos.add(slope) {
                    Ok(_) => {},
                    Err(_) => break,
                }
                match self.get(&pos) {
                    Obstacle::Empty => {},
                    Obstacle::Tree => trees += 1,
                }
            }
            Ok(trees)
        }
    }
}

impl Point {
    pub fn new(x: usize, y: usize, max_x: usize, max_y: usize) -> Point {
        let cleaned_x: usize;
        let cleaned_y: usize;
        if max_x == 0 {
            cleaned_x = usize::MAX;
        } else {
            cleaned_x = max_x;
        }
        if max_y == 0 {
            cleaned_y = usize::MAX;
        } else {
            cleaned_y = max_y;
        }

        Point{
            x: x,
            y: y,
            max_x: cleaned_x,
            max_y: cleaned_y,
        }
    }

    pub fn add(&mut self, movement: &Point) -> Result<(), ()> {
        let mut new_x = self.x + movement.x;
        let new_y = self.y + movement.y;
        if new_y > self.max_y {
            Err(())
        } else {
            if new_x > self.max_x {
                new_x = new_x - self.max_x - 1;
            }
            self.x = new_x;
            self.y = new_y;
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_point_zeros() {
        let expected = Point{x: 0, y:0, max_x: std::usize::MAX, max_y: std::usize::MAX};
        assert_eq!(Point::new(0,0,0,0), expected)
    }

    #[test]
    fn test_new_point() {
        let expected = Point{x: 5, y:0, max_x: 7, max_y: 8};
        assert_eq!(Point::new(5,0,7,8), expected)
    }

    #[test]
    fn test_point_add() {
        let mut point = Point::new(0,0,5,5);
        match point.add(&Point::new(1,1,0,0)) {
            Ok(_) => {},
            Err(_) => assert!(false),
        }
        assert_eq!(point, Point::new(1,1,5,5))
    }

    #[test]
    fn test_point_add_x_overflow() {
        let mut point = Point::new(0,0,5,5);
        match point.add(&Point::new(6,1,0,0)) {
            Ok(_) => {},
            Err(_) => assert!(false),
        }
        assert_eq!(point, Point::new(0,1,5,5))
    }

    #[test]
    fn test_point_add_y_overflow() {
        let mut point = Point::new(0,0,5,5);
        match point.add(&Point::new(1,6,0,0)) {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
    }
}
